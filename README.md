# Portfolio de Paulrubens

![Bannière du portfolio](./rsc/banner.png)

## 📑 Table des matières

- [Introduction](#introduction)
- [Fonctionnalités](#fonctionnalités)
- [Technologies utilisées](#technologies-utilisées)
- [Sections](#sections)

---

## 🎉 Introduction

Bienvenue sur le dépôt de mon portfolio personnel. Ce portfolio présente un aperçu de mon parcours professionnel et académique, y compris mes projets et mon CV.

Pour voir le portfolio en action, visitez [Portfolio de Paulrubens](https://paulrubens.gitlab.io/portfolio/).

## ✨ Fonctionnalités

- Interface utilisateur intuitive et réactive
- Sections diverses pour une meilleure organisation de l'information :
    - Introduction
    - Études
    - Projets
    - CV
    - Contact

## 🛠 Technologies utilisées

- HTML5
- CSS3
- JavaScript
- Materialize CSS
- Google Fonts

## 📁 Sections

- **Introduction**: Une brève introduction à mon portfolio.
- **Études**: Informations sur mes études.
- **Projets**: Une galerie de projets sur lesquels j'ai travaillé, avec des liens vers les codes sources.
- **CV**: Un lien vers mon CV complet.
- **Contact**: Informations pour me contacter.

---
